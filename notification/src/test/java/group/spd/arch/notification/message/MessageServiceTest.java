package group.spd.arch.notification.message;

import group.spd.arch.error.NotFoundException;
import group.spd.arch.error.ValidationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {

    @Mock
    private MessageRepository messageRepository;

    @InjectMocks
    private MessageService messageService;

    @Test
    @DisplayName("Should throw not found when try to get not existing message")
    void testGetNotExisting() {
        when(messageRepository.getById(anyInt())).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> messageService.getById(42));
    }

    @Test
    @DisplayName("Should return message from DB if it was found")
    void testSuccessGet() {
        final Message expectedMessage = new Message();
        when(messageRepository.getById(anyInt())).thenReturn(Optional.of(expectedMessage));

        final Message loadedMessage = messageService.getById(42);
        assertSame(expectedMessage, loadedMessage);
    }

    @Test
    @DisplayName("Should fail validation for empty type on creation")
    void validateTypeOnCreate() {
        final Message message = validMessage();
        message.setType("   ");
        assertThrows(ValidationException.class, () -> messageService.create(message));
    }

    @Test
    @DisplayName("Should fail validation for empty content on creation")
    void validateContentOnCreate() {
        final Message message = validMessage();
        message.setContent("   ");
        assertThrows(ValidationException.class, () -> messageService.create(message));
    }

    @Test
    @DisplayName("Should save valid message")
    void testCreation() {
        final Message message = validMessage();
        messageService.create(message);
        verify(messageRepository).save(message);
    }

    @Test
    @DisplayName("Should fail validation for empty type on update")
    void validateTypeOnUpdate() {
        final Message message = validMessage();
        message.setType("   ");
        assertThrows(ValidationException.class, () -> messageService.update(message));
    }

    @Test
    @DisplayName("Should fail validation for empty content on update")
    void validateContentOnUpdate() {
        final Message message = validMessage();
        message.setContent("   ");
        assertThrows(ValidationException.class, () -> messageService.update(message));
    }

    @Test
    @DisplayName("Should throw not found if can't find message for update")
    void notExistingMessageForUpdate() {
        final Message message = validMessage();
        when(messageRepository.update(message)).thenReturn(0);
        assertThrows(NotFoundException.class, () -> messageService.update(message));
    }

    @Test
    @DisplayName("Should update valid message")
    void testUpdate() {
        final Message message = validMessage();
        when(messageRepository.update(message)).thenReturn(1);

        messageService.update(message);
        verify(messageRepository).update(message);
    }

    private Message validMessage() {
        final Message message = new Message();
        message.setType("SomeType");
        message.setContent("This is message content");
        return message;
    }
}