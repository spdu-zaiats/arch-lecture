package group.spd.arch.notification.message;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public Message getById(int id) {
        return messageService.getById(id);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Message createMessage(@RequestBody Message message) {
        return messageService.create(message);
    }

    @PutMapping
    public void updateMessage(@RequestBody Message message) {
        messageService.update(message);
    }
}
