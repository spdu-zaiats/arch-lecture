package group.spd.arch.notification.message;

import java.util.Optional;

public interface TypedMessageProvider {

    Optional<Message> getByType(String type);
}
