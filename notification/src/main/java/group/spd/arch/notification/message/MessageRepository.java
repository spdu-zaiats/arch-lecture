package group.spd.arch.notification.message;

import java.util.Optional;

public interface MessageRepository {

    Optional<Message> getById(int id);

    Message save(Message message);

    int update(Message message);
}
