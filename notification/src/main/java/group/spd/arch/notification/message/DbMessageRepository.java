package group.spd.arch.notification.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

@Repository
public class DbMessageRepository implements MessageRepository, TypedMessageProvider{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    public DbMessageRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("messages")
                .usingGeneratedKeyColumns("id");
    }

    @Override
    public Optional<Message> getById(int id) {
        final String sql = "select * from messages where id = ?";
        try {
            final Message message = jdbcTemplate.queryForObject(sql, this::toMessage, id);
            return Optional.ofNullable(message);
        } catch (DataAccessException e) {
            logger.error(e.getMessage(), e);
            return Optional.empty();
        }
    }

    @Override
    public Message save(Message message) {
        Map<String, Object> params = Map.of(
                "type", message.getType(),
                "content", message.getContent()
        );
        final Number id = jdbcInsert.executeAndReturnKey(params);
        message.setId(id.intValue());
        return message;
    }

    @Override
    public int update(Message message) {
        final String sql = "update messages set type = ?, content = ? where id = ?";
        return jdbcTemplate.update(sql, message.getType(), message.getContent(), message.getId());
    }

    @Override
    public Optional<Message> getByType(String type) {
        final String sql = "select * from messages where type = ?";
        try {
            final Message message = jdbcTemplate.queryForObject(sql, this::toMessage, type);
            return Optional.ofNullable(message);
        } catch (DataAccessException e) {
            logger.error(e.getMessage(), e);
            return Optional.empty();
        }
    }

    private Message toMessage(ResultSet rs, int rowNum) throws SQLException {
        final Message message = new Message();
        message.setId(rs.getInt("id"));
        message.setType(rs.getString("type"));
        message.setContent(rs.getString("content"));
        return message;
    }
}
