package group.spd.arch.notification;

import group.spd.arch.notification.sender.NotificationSendEvent;
import group.spd.arch.task.TaskCreatedEvent;
import group.spd.arch.task.TaskNotificationSender;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class NotificationService implements TaskNotificationSender {

    private final RabbitTemplate eventPublisher;

    public NotificationService(RabbitTemplate eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void onTaskCreated(TaskCreatedEvent taskCreatedEvent) {
        final Map<String, String> params = Map.of(
                "description", taskCreatedEvent.getTaskDescription(),
                "deadline", taskCreatedEvent.getDeadline().toString()
        );
        final NotificationSendEvent event = new NotificationSendEvent("TaskCreated", params, taskCreatedEvent.getAssigneeEmail());
        eventPublisher.convertAndSend("NotificationQueue", event);
    }
}
