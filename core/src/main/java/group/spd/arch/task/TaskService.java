package group.spd.arch.task;

import group.spd.arch.error.ConflictException;
import group.spd.arch.error.ValidationException;
import group.spd.arch.person.Person;
import group.spd.arch.person.PersonProvider;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static java.time.LocalDate.now;

@Service
public class TaskService {

	private final TaskRepository taskRepository;
	private final PersonProvider personProvider;
	private final TaskNotificationSender notificationService;

	public TaskService(TaskRepository taskRepository, PersonProvider personProvider, TaskNotificationSender notificationService) {
		this.taskRepository = taskRepository;
		this.personProvider = personProvider;
		this.notificationService = notificationService;
	}

	public List<Task> getAll() {
		return taskRepository.getAll();
	}

	public Task save(Task task) {
		validate(task);
		final int assigneeId = task.getAssigneeId();
		final Person assignee = personProvider.getById(assigneeId)
				.orElseThrow(() -> new ConflictException("Can't find assignee with id + " + assigneeId));
		sendNotification(task, assignee);
		return taskRepository.save(task);
	}

	private void validate(Task task) {
		if (task.getDescription() == null || task.getDescription().isBlank()) {
			throw new ValidationException("Description is required");
		}

		final LocalDate deadline = task.getDeadline();
		if (deadline == null) {
			throw new ValidationException("Deadline is required");
		}
		if (deadline.isBefore(now())) {
			throw new ValidationException("Deadline can't be in the past.");
		}
	}

	private void sendNotification(Task task, Person recipient) {
		final TaskCreatedEvent event = new TaskCreatedEvent(task.getDescription(), task.getDeadline(), recipient.getEmail());
		notificationService.onTaskCreated(event);
	}
}
