package group.spd.arch.task;

public interface TaskNotificationSender {

    void onTaskCreated(TaskCreatedEvent event);
}
