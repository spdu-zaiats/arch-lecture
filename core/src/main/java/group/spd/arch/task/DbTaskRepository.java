package group.spd.arch.task;

import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DbTaskRepository implements TaskRepository {

    private final DataSource dataSource;

    public DbTaskRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Task save(Task task) {
        final String sql = "insert into tasks(description, deadline, assignee_id) VALUES (?, ?, ?)";

        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, task.getDescription());
            ps.setDate(2, Date.valueOf(task.getDeadline()));
            ps.setInt(3, task.getAssigneeId());
            ps.executeUpdate();

            try (final ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                task.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return task;
    }

    @Override
    public List<Task> getAll() {
        final List<Task> tasks = new ArrayList<>();
        final String sql = "select * from tasks";

        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement statement = connection.prepareStatement(sql);
             final ResultSet rs = statement.executeQuery()) {
            while (rs.next()) {
                final Task task = new Task();
                task.setId(rs.getInt("id"));
                task.setDescription(rs.getString("description"));
                task.setDeadline(rs.getDate("deadline").toLocalDate());
                task.setAssigneeId(rs.getInt("assignee_id"));
                tasks.add(task);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return tasks;
    }
}
