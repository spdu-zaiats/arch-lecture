package group.spd.arch.person;

import group.spd.arch.error.ValidationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

	private final PersonRepository repository;

	public PersonService(PersonRepository repository) {
		this.repository = repository;
	}

	public List<Person> getAll() {
		return repository.getAll();
	}

	public Person save(Person person) {
		validate(person);
		return repository.save(person);
	}

	private void validate(Person person) {
		if (person.getName() == null || person.getName().isBlank()) {
			throw new ValidationException("Name is required.");
		}
		if (person.getEmail() == null || person.getEmail().isBlank()) {
			throw new ValidationException("Email is required.");
		}
	}
}
